Login into Google cloud with
```
gcloud auth login
```

Create and set project
```
gcloud projects create <project-name> --enable-cloud-apis
gcloud config set project  <project-name> 
```

If needed link the project to a billing adress otherwise you are unable to enable the compute and container api's
```
gcloud alpha billing accounts list
gcloud beta billing projects link  this-is-a-second-test-aaaaa --billing-account=<ACCOUNT_ID>
```

Or if only 1 billing account is enabled
```
gcloud beta billing projects link  this-is-a-second-test-aaaaa --billing-account=$(gcloud alpha billing accounts list |  tail -n1 | awk  '{print $1}')
```


Enable the needed API endpoints 
```
gcloud services enable iam.googleapis.com
gcloud services enable compute.googleapis.com
gcloud services enable container.googleapis.com
gcloud services enable cloudresourcemanager.googleapis.com
```

Create Service account
```
gcloud iam service-accounts create <service_account_id> --display-name="DISPLAY_NAME"
gcloud projects add-iam-policy-binding PROJECT_ID --member="serviceAccount:<service_account_id>@<project-name>.iam.gserviceaccount.com" --role="roles/owner" 
```

Create authentication key needed for terraform
``` 
gcloud iam service-accounts keys create auth.json  --iam-account=<service_account_id>@<project-name>.iam.gserviceaccount.com
```

Create variables.auto.tfvars with the correct configuration

example 
```
gke_credentials = "auth.json"
gke_service_account = "<service_account_id>@<project-name>.iam.gserviceaccount.com"   
gke_project_id = "<project-name>"
gke_region = "europe-west4"
gke_cluster_name = "my-first-cluster"
gke_zones = ["europe-west4-a","europe-west4-b"]
gke_nodepool_name = "cluster-node-pool"
```

Run terraform 
```
terraform init
terraform plan
terraform apply
```

Get kube config file 
```
gcloud container clusters get-credentials <gke_cluster_name> --zone europe-west4-a
```

Change the password in ./wordpress/kustomization.yaml and apply mysql and wordpress deployment with

```
kubectl apply  -k  ./wordpress
```