data "google_client_config" "default" {}

provider "google" {
    credentials = file(var.gke_credentials)
    project = var.gke_project_id
    region = var.gke_region
}

provider "kubernetes" {
  host                   = "https://${module.gke.endpoint}"
  token                  = data.google_client_config.default.access_token
  cluster_ca_certificate = base64decode(module.gke.ca_certificate)
}