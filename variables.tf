variable "gke_credentials" {
    type        = string
    default     = "auth.json"
    description = "google credential json file"
}


variable "gke_project_id" {
    type        = string
    description = "Google project ID"
}

variable "gke_region" {
    type        = string
    default     = "europe-west4"
    description = "Google region to deploy"
}

variable "gke_cluster_name"{
    type        = string
    description = "K8S clustername "
}


variable "gke_regional"{
    type        = bool
    default     = false
    description = "K8S use regional cluster"
}

variable "gke_zones"{
    type        = list(string)
    default     = ["europe-west4-a","europe-west4-b"]
    description = "List of GKE cluster zones "
}

variable "gke_network"{
    type        = string
    default     = "default"
    description = "VPC network"
}

variable "gke_subnetwork"{
    type        = string
    default     = "default"
    description = "VPC sub network"
}

variable "gke_nodepool_name"{
    type        = string
    default     = "default-node-pool"
    description = "GKE Default node pool name"
}

variable "gke_service_account"{
    type        = string
    description = "GKE service account"
}

